package Filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
//@WebFilter("/*")
public class DoFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request=(HttpServletRequest) servletRequest;
        String path=request.getServletPath();
        if (path.equals("/login.jsp")||path.equals("/dogin.jsp")||path.endsWith(".js")||path.endsWith(".css")||path.endsWith(".png")){
            filterChain.doFilter(request,servletResponse);
            return;
        }

        Object uname=request.getSession().getAttribute("uname");

        if(uname==null){
            request.setAttribute("msg","密码或用户名错误");
            request.getRequestDispatcher("/login.jsp").forward(request,servletResponse);
        }else {
            filterChain.doFilter(request,servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
