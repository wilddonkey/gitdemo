package cn.java71.servlet.ctrl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author oldliu
 * @since 1.0
 */
//每个模块的功能，入口在mainservlet
public class UserController implements Filter{
    public void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("这是修改user");
    }

}
