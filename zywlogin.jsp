<%@ page import="Class1.Admin" %><%--
  Created by IntelliJ IDEA.
  User: ASUS
  Date: 2021/11/24
  Time: 18:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>管理系统</title>
    <style>
        #cont div{
            text-align: center;
            padding: 15px;
        }
    </style>
</head>
<body>
<jsp:include page="top.jsp"/>

<div id="cont">
    <%
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        Admin admin = (Admin) request.getSession().getAttribute("user");
        String msg = (String) request.getSession().getAttribute("msg");
        if (msg!=null){%>
          <h3 style="color: red">${msg}</h3>
        <%
        session.removeAttribute(msg);
        }
    %>
<form action="<%=request.getContextPath()%>/loginServlet?type=truelogin" method="post">

<div>
    <p><label for="a">用户:</label><input type="text" name="username" id="a"></p>
    <p><label for="b">密码:</label><input type="password" name="password"id="b"></p>
    <p><a href="<%=request.getContextPath()%>/toStudentlogin.StudentServlet?type=toStudentlogin">学生登入</a></p>
    <p><input type="submit" value="登入"></p>
</div>
</form>

</div>

<jsp:include page="bottom.jsp"/>
</body>
</html>
