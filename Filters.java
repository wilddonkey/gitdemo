package Filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
//@WebFilter("/*")
public class Filters implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("进入Filter init");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        Long start=System.nanoTime();

        HttpServletRequest request =(HttpServletRequest) servletRequest;
        String path=request.getServletPath();
        System.out.println("当前访问地址："+path);
        request.setCharacterEncoding("utf-8");
        servletResponse.setContentType("text/html;charset=utf-8");
        //放行
        filterChain.doFilter(request,servletResponse);
        Long end=System.nanoTime();
        Long s=end-start;
        System.out.println("访问的时间："+s);
    }

    @Override
    public void destroy() {
        System.out.println("销毁Filter destroy");
    }
}
