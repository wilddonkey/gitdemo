package cn.laoliu.esclient.esmapper;

import cn.easyes.core.conditions.interfaces.BaseEsMapper;
import cn.laoliu.esclient.entity.Questions;

public interface QuestionESMapper extends BaseEsMapper<Questions> {
}
