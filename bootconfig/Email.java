package cn.java79.springboot.zsb.config;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author oldliu
 * @since 1.0
 */
@Data
@ToString
@Component("email")
@ConfigurationProperties(prefix = "myemail")
public class Email {
    private String host;
    private int port;
    private String emailAccount;
    private String pass;//密码

    private String[] fileTypes=new String[0];

    private Map<String,String> params=new HashMap<>();

}
