package cn.ruituo.test;

import cn.ruituo.dao.examresult.*;
import cn.ruituo.entity.*;
import cn.ruituo.util.*;
import org.apache.ibatis.session.*;

import java.util.*;

public class TestExamResultDao {
    public static void main(String[] args) {
        SqlSession sqlSession = MyBatisUtils.openSession();
        ExamResultDao dao = sqlSession.getMapper(ExamResultDao.class);

        List<ExamResult> all1 = dao.findAll1();
        for (ExamResult examResult : all1) {
            LogUtil.info(examResult);
        }
        
        MyBatisUtils.close(sqlSession);
    }
}
